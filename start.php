<?php
include 'source\autoload.php';

$data = array(
	array(
		'transport'   => 'bus',
		'source'      => 'Barcelona',
		'destination' => 'Gerona',
		'number'      => '12345',
		'seat'        => null,
	),
	array(
		'transport'   => 'plane',
		'source'      => 'Gerona',
		'destination' => 'Stockholm',
		'number'      => 'SK455',
		'seat'        => '3A',
		'gate'        => '45B',
	),
	array(
		'transport'   => 'train',
		'source'      => 'Madrid',
		'destination' => 'Barcelona',
		'number'      => '78A',
		'seat'        => '45B',
	),
	array(
		'transport'   => 'plane',
		'source'      => 'Stockholm',
		'destination' => 'New York',
		'number'      => 'SK22',
		'seat'        => '7B',
		'gate'        => '45B',
		'baggage'     => '333',
	),
);
		
// create new trip with specific tickets
$trip = new Trip($data);

// find path 
$paths = $trip->find();

// print path to screen
if (count($paths)) {
	foreach($paths as $path) {
		echo $path->getInfo() . "\n";
	}
} else {
	echo 'Path not found.';
}
