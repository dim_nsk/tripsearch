<?php
if (version_compare(PHP_VERSION, '5.3.0', '<')) {
	exit('Sorry, you must have PHP version 5.3 or above');
}

function tripAutoloader($class) {
	$fileName = implode(DIRECTORY_SEPARATOR, array(dirname(__FILE__), 'classes', str_replace('\\', DIRECTORY_SEPARATOR, $class . '.php')));
	if (is_readable($fileName)) {
		require_once $fileName;
	}
}
spl_autoload_register('tripAutoloader');
