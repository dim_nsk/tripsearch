<?php

/**
 * Trip class
 */
class Trip {
	
	/**
	 *	Tickets list
	 * 
	 * @var array
	 */
	protected $list = array();
	
	/**
	 * Constructor
	 * 
	 * @param array $data
	 * @throws \TripException
	 */
	public function __construct($data) {
		if (!count($data)) {
			throw new \TripException('Can\'t init trip.');
		}
		
		foreach ($data as $item) { 
			$this->list[$item['source']] = Transport\TransportFactory::build($item);
		}
	}
	
	/**
	 * Find path from $source to $destination
	 * 
	 * @return array
	 */
	public function find() {

		$res = array();
		$sourceIdx = array();
		$destinationIdx = array();
		
		// create indexes for source and destination
		foreach($this->list as $ticket) {
			$sourceIdx[$ticket->source] = 1;
			$destinationIdx[$ticket->destination] = 1;
		}
			
		// find start place
		foreach($this->list as $ticket) {
			if (!isset($destinationIdx[$ticket->source])) {
				$start = $ticket->source;
				break;
			}
		}
		
		// make consecutive list of tickets
		if (isset($start)) {
			while($start) {
				if (!isset($sourceIdx[$start])) {
					break;
				}
				$res[] = $this->list[$start];
				$start = $this->list[$start]->destination;
			}
		}
		return $res;
	}
	
	/**
	 * Get list of tickets
	 * 
	 * @return array
	 */
	public function getList() {
		return $this->list;
	}
}