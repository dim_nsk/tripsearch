<?php
namespace Transport;

/**
 * Transport factory class
 */
class TransportFactory {

    /**
	 * Create transport object
	 * 
	 * @param array $data
	 * @return \Transport\className
	 * @throws \TripException
	 */
	public static function build(array $data) {
		
		$className = 'Transport\\Transport' . ucfirst($data['transport']);
		if (class_exists($className)) {
			return new $className($data);
		} else {
			throw new \TripException("Invalid transport type given.");
		}
	}
}
