<?php
namespace Transport;

/**
 * Bus transport class
 */
class TransportBus extends TransportAbstract implements ITransport{
	
	public $number;
	public $seat;
	
	/**
	 * Constructor
	 * 
	 * @param array $data
	 * @throws \TripException
	 */
	public function __construct($data) {
		if (empty($data['number'])) {
			throw new \TripException('Bus number is not defined.');
		}
		$this->number = $data['number'];
		$this->seat = isset($data['seat']) ? $data['seat'] : null;
		parent::__construct($data);
	}
	
	/**
	 * Get info about ticket
	 * 
	 * @return string
	 */
	public function getInfo() {
		$res = 'Take bus from ' . $this->source . ' to ' . $this->destination .
				'. Bus number is ' . $this->number . '.';
		if ($this->seat) {
			$res .= ' Seat number is ' . $this->seat;
		}
		return $res;
	}
}
