<?php
namespace Transport;

/**
 * Transport interface
 */
interface ITransport {
	public function getInfo();
}
