<?php
namespace Transport;

/**
 * Transport abstract class
 */
class TransportAbstract {
	
	public $transport;
	public $source;
	public $destination;
	
	/**
	 * Constructor
	 * 
	 * @param array $data
	 */
	public function __construct($data) {
		$this->transport = $data['transport'];
		$this->source = $data['source'];
		$this->destination = $data['destination'];
	}
	
}
