<?php
namespace Transport;

/**
 * Plane transport class
 */
class TransportPlane extends TransportAbstract implements ITransport {
	
	public $number;
	public $seat;
	public $baggage;
	
	/**
	 * Constructor
	 * 
	 * @param array $data
	 * @throws \TripException
	 */
	public function __construct($data) {
		if (empty($data['number'])) {
			throw new \TripException('Flight number is not defined.');
		}
		if (empty($data['seat'])) {
			throw new \TripException('Flight seat is not defined.');
		}
		$this->number = $data['number'];
		$this->seat = $data['seat'];
		$this->baggage = isset($data['baggage']) ? $data['baggage'] : null;
		parent::__construct($data);
	}
	
	/**
	 * Get info about ticket
	 * 
	 * @return string
	 */
	public function getInfo() {
		$res = 'Take plane from ' . $this->source . ' to ' . $this->destination .
				'. Flight number is ' . $this->number . '.';
		$res .= ' Seat number is ' . $this->seat;
		if ($this->baggage) {
			$res .= ' Baggage number is ' . $this->baggage;
		}
		return $res;
	}
}
