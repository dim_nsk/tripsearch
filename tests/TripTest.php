<?php

/**
 * Trip unit test
 */
class TripTest extends  PHPUnit_Framework_TestCase {
	
	protected $trip;
	
	// data fixtures
	public $data = array(
		array(
			'transport'   => 'bus',
			'source'      => 'Barcelona',
			'destination' => 'Gerona',
			'number'      => '12345',
			'seat'        => null,
		),
		array(
			'transport'   => 'plane',
			'source'      => 'Gerona',
			'destination' => 'Stockholm',
			'number'      => 'SK455',
			'seat'        => '3A',
			'gate'        => '45B',
		),
		array(
			'transport'   => 'train',
			'source'      => 'Madrid',
			'destination' => 'Barcelona',
			'number'      => '78A',
			'seat'        => '45B',
		),
		array(
			'transport'   => 'plane',
			'source'      => 'Stockholm',
			'destination' => 'New York',
			'number'      => 'SK22',
			'seat'        => '7B',
			'gate'        => '45B',
			'baggage'     => '333',
		),
	);
	
	/**
	 * Setup method
	 */
	protected function setUp() {
		$this->trip = new Trip($this->data);
	}
	
    /**
	 * Test correct creation of objects
	 */
	public function testObjectsCreation() {
		
		$this->assertInstanceOf('Trip', $this->trip);
		$list = $this->trip->getList();
		
		foreach($this->data as $item) {
			$this->assertArrayHasKey($item['source'], $list);
			$this->assertInstanceOf('Transport\\Transport' . ucfirst($item['transport']), $list[$item['source']]);
		}
    }
	
	/**
	 * Test for find method with positive scenario
	 */
	public function testFindPositive() {
		
		$paths = $this->trip->find();
		$this->assertEquals(4, count($paths));
	}
	
	/**
	 * Test for find method with exceptions
	 */
	public function testFindException() {
		
		try {
			$this->trip = new Trip(array());
			$this->fail("Expected exception not thrown");			
		} catch(TripException $e) {}
			
		try {
			$data = array(
				array(
					'transport' => 'wrong',
					'source' => 'City1',
					'destination' => 'City2',
				),
			);
			$this->trip = new Trip($data);
			$this->fail("Expected exception not thrown");			
		} catch(TripException $e) {}
		
		try {
			$data = array(
				array(
					'transport' => 'bus',
					'source' => 'City1',
					'destination' => 'City2',
					'number' => '',
				),
			);
			$this->trip = new Trip($data);
			$this->fail("Expected exception not thrown");			
		} catch(TripException $e) {}
		
		try {
			$data = array(
				array(
					'transport' => 'train',
					'source' => 'City1',
					'destination' => 'City2',
					'number' => '',
				),
			);
			$this->trip = new Trip($data);
			$this->fail("Expected exception not thrown");			
		} catch(TripException $e) {}
		
		try {
			$data = array(
				array(
					'transport' => 'plane',
					'source' => 'City1',
					'destination' => 'City2',
					'number' => '',
				),
			);
			$this->trip = new Trip($data);
			$this->fail("Expected exception not thrown");			
		} catch(TripException $e) {}
		
		try {
			$data = array(
				array(
					'transport' => 'plane',
					'source' => 'City1',
					'destination' => 'City2',
					'number' => '123',
					'seat' => '',
				),
			);
			$this->trip = new Trip($data);
			$this->fail("Expected exception not thrown");			
		} catch(TripException $e) {}
	}
	
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                